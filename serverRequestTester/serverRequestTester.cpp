// serverRequestTester.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "WinHttpClient.h"
#include "INI.h"
#include "sha256.h"
#include "resource.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

// Windows Library Files:
#pragma comment(lib, "ws2_32.lib")

// open up auth file
File auth_file("luxx\\auth.ini");
//Set access to my remote functions server
network remoteServer(L"http://luxx.menu/remote/remote.php");

//store last string to stop duplicates in game
std::string lastNotice = ""; 
std::string verify = "verify=" + static_cast<std::string>("ddbde340b6cd8fed42a482dd9811a9aaf82c73c7e4cb47de53bb41fedad98c34");
std::string tokenD = "7ed4b33fd88d6daa80ce359ec4df60d1";

int options = 0;

void tsOut(std::string out) {
	time_t now = time(0);
	tm t;
	localtime_s(&t, &now);
	ostringstream i1, i2, i3;
	i1 << t.tm_sec; i2 << t.tm_min; i3 << t.tm_hour;

	std::string s1 = i1.str(), s2 = i2.str(), s3 = i3.str();

	if (t.tm_sec < 10)
		s1 = "0"s + i1.str();
	if (t.tm_min < 10)
		s2 = "0"s + i2.str();
	if (t.tm_hour < 10)
		s3 = "0"s + i3.str();

	std::string output =	" "s + s3 + ":"s + s2 + ":"s + s1 + "  - "s;
	" 01:14:06  - ";
	cout << output << out << "\n" << std::flush;
}

void AddOption(std::string option) {
	ostringstream i1;
	i1 <<options;
	cout << " Option "s + i1.str() + "  - "s + option << "\n" << std::flush;
	options++;
}
std::string serverCall(std::string request) {

	//build post message using request data
	std::string requestType = "request=" + request;
	std::string postMsg = requestType + "&" + verify;

	//recieve info matching notice
	std::wstring response = remoteServer.post(postMsg);

	//cast wstring response to string
	std::string str_response; str_response.assign(response.begin(), response.end());

	return str_response;
}
std::string serverCall(std::string request, std::string token) {

	//build post message using request data
	std::string userToken = "token=" + token; //to be replaced with token grab function
	std::string postMsg = request + "&" + userToken;

	//call standard function
	std::string response = serverCall(postMsg);//; str_response.assign(response.begin(), response.end());

	return response;
}
int ticks;
void pingServer() {

	tsOut("Pinging Server. . .");
	ticks = GetTickCount();
	if (serverCall("ping") == "Pong.") {
		tsOut("Pong.");
		ostringstream st;
		st << GetTickCount() - ticks;
		tsOut("Server Round Trip took " + st.str() + " ticks.");
	}
	else
		tsOut("Server Unavailable");
}
int loops;
void pingServerC() {

	for (int i = 0; i < 10; i++) {
		bool b = false;
		if (GetTickCount() > ticks) {
			b = true;
			ticks = GetTickCount() + 2500;
		}
	}
}
std::string Chwid;
void setHwid(){

	char hwid[4096];

	// total physical memory
	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof(statex);
	GlobalMemoryStatusEx(&statex);
	sprintf_s(hwid, "%I64i", statex.ullTotalPhys / 1024);

	// volume information
	TCHAR volume_name[MAX_PATH + 1] = { 0 };
	TCHAR filesystem_name[MAX_PATH + 1] = { 0 };
	DWORD serial_number = 0, maxcomponentlen = 0, filesystemflags = 0;
	GetVolumeInformation(_T("C:\\"), volume_name, ARRAYSIZE(volume_name), &serial_number, &maxcomponentlen, &filesystemflags, filesystem_name, ARRAYSIZE(filesystem_name));
	sprintf_s(hwid, "%s%li%ws%li", hwid, serial_number, filesystem_name, maxcomponentlen);

	// computer name
	TCHAR computer_name[MAX_COMPUTERNAME_LENGTH + 1];
	DWORD size = sizeof(computer_name) / sizeof(computer_name[0]);
	GetComputerName(computer_name, &size);
	sprintf_s(hwid, "%s%ws", hwid, computer_name);

	// cast the hwid we made to string
	Chwid = static_cast<std::string>(hwid);

	tsOut("HWID: "s + Chwid + " stored.");
}

bool checkNotices(std::string *notice) {

	//NOTE: There is a very large limit on the max length that I can receive. However I will limit the display length before anything else.

	//get response from server
	std::string response = serverCall("notice");

	if (response != "Blank") {
		*(std::string*)notice = response;
		if (!(response == lastNotice)) {
			lastNotice = response;
			return true;
		}
		else
			tsOut("No new Server Messages");
	}
	else {
		*(std::string*)notice = "";
		tsOut(response);
	}
	return false;
}

void checkNotices() {

	//create notice for future use
	std::string notice;

	if (checkNotices(&notice)) {
		tsOut(notice); //when using with luxx, change this to game::notifyError(str_response); and remove error from the start of the response.
	}
}

void checkUpdate() {

	cout << "  _    _           _       _      ___  "s << "\n" << std::flush;
	cout << " | |  | |         | |     | |    |__ \\ "s << "\n" << std::flush;
	cout << " | |  | |_ __   __| | __ _| |_ ___  ) |"s << "\n" << std::flush;
	cout << " | |  | | '_ \\ / _` |/ _` | __/ _ \\/ / "s << "\n" << std::flush;
	cout << " | |__| | |_) | (_| | (_| | ||  __/_|  "s << "\n" << std::flush;
	cout << "  \\____/| .__/ \\__,_|\\__,_|\\__\\___(_)  "s << "\n" << std::flush;
	cout << "        | |                            "s << "\n" << std::flush;
	cout << "        |_|                            "s << "\n\n" << std::flush;

	time_t now = time(0);
	tm t;
	localtime_s(&t, &now);
	ostringstream i1, i2, i3;
	i1 << t.tm_sec; i2 << t.tm_min; i3 << t.tm_hour;

	std::string s1 = i1.str(), s2 = i2.str(), s3 = i3.str();

	if (t.tm_sec < 10)
		s1 = "0"s + i1.str();
	if (t.tm_min < 10)
		s2 = "0"s + i2.str();
	if (t.tm_hour < 10)
		s3 = "0"s + i3.str();

	std::string output = " As of "s + s3 + ":" + s2 + ":" + s1 + " the answer is: "s;

	cout << output << serverCall("update") << "\n\n" << std::flush;
}

std::vector<std::wstring> responses;
bool is_cracked;
std::string imfat[]{
	"WbdYpWaY8GFGtCLyJ",
	"EtjZbTz5yde6RxHFu",
	"rs7SfNKDQjHcfHRD5",
	"FU5R2Tds88a39SHdf",
	"mtguYQe5kVthPuu3e",
	"ZnxmjmBAAxwwHe6eT",
	"w3DVqKhfg2UXJP6kJ",
	"fBs5bjUuP34tmRKEA",
	"fYzJB235nQvLPN7nh",
	"muzn4ZJhYpyGbQBMp",
	"qDfZsaBTpgNffEdxD",
	"4kgRhXeN3uReKZ2ww",
	"JesBFthkY8azt37sz",
	"MJrWrDJhNTukGaFAp",
	"L2dXUB3y9hYpHkuUj",
	"Y7PPbnveTvVau7NB3",
	"wCDyCfa5HwzW6zv3m",
	"R7J2FBt75j2WS8c5v",
	"K7UxBfT62XfH2SEWf"
};

void run_auth()
{
	is_cracked = false;

	//network server(L"https://luxx.menu/test_auth_diredan/auth.php");
	network server(L"https://luxx.menu/test_auth_diredan/multi_level_test/multi_level_auth.php");

	char hwid[4096];

	// total physical memory
	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof(statex);
	GlobalMemoryStatusEx(&statex);
	sprintf_s(hwid, "%I64i", statex.ullTotalPhys / 1024);

	// volume information
	TCHAR volume_name[MAX_PATH + 1] = { 0 };
	TCHAR filesystem_name[MAX_PATH + 1] = { 0 };
	DWORD serial_number = 0, maxcomponentlen = 0, filesystemflags = 0;
	GetVolumeInformation(_T("C:\\"), volume_name, ARRAYSIZE(volume_name), &serial_number, &maxcomponentlen, &filesystemflags, filesystem_name, ARRAYSIZE(filesystem_name));
	sprintf_s(hwid, "%s%li%ws%li", hwid, serial_number, filesystem_name, maxcomponentlen);

	// computer name
	TCHAR computer_name[MAX_COMPUTERNAME_LENGTH + 1];
	DWORD size = sizeof(computer_name) / sizeof(computer_name[0]);
	GetComputerName(computer_name, &size);
	sprintf_s(hwid, "%s%ws", hwid, computer_name);

	// hash the hwid we made
	std::string hashed_hwid_string = sha256(static_cast<std::string>(hwid));

	// get user socialclub id
	const char* socialclub_id = "luxx.luxx";

	// open up auth file
	//File auth_file("luxx\\auth.ini");
	// get their discord, token and construct the post message
	//std::string discord = "opal#3499"s, token = tokenD, message = "scid=" + static_cast<std::string>(socialclub_id) + "&hwid=" + hashed_hwid_string + "&" + discord + "&" + token;
	//Log::Msg(const_cast<char*>(message.c_str()));

	// open up auth file
	File auth_file("luxx\\auth.ini");
	// get their discord, token and construct the post message
	std::string discord = auth_file.readLine(1), token = auth_file.readLine(2), message = "scid="s + static_cast<std::string>(socialclub_id) + "&hwid="s + hashed_hwid_string + "&reference="s + "opal#3499"s + "&token="s + tokenD;
	tsOut(message);

	// make the post request
	std::wstring response = server.post(message);

	// check if the response has been made before, we only allow 2 same responses
	static int repeat_count = 0;
	for (auto &check : responses) {
		if (check == response) {
			repeat_count += 1;
			if (repeat_count == 2) {
				tsOut("Dulplicate Response Received, Forcing Exit");
				exit(0);
			}
		}
	}
	// add this response into the response vector
	responses.push_back(response);

	// cast to string
	std::string str_response; str_response.assign(response.begin(), response.end());
	//Log::Msg(const_cast<char*>(str_response.c_str()));

	if (str_response.find(hashed_hwid_string) == std::string::npos) {
		tsOut(str_response); //when using with luxx, change this to game::notifyError(str_response); and remove error from the start of the response.
		return;
	}

	// split the response into a vector
	std::size_t position = 0; std::string split_token; std::vector<std::string> contents;
	while ((position = str_response.find("q")) != std::string::npos) {
		split_token = str_response.substr(0, position);
		contents.push_back(split_token);
		str_response.erase(0, position + 1);
	}

	contents.push_back(str_response);
	tsOut("Split into the vector");

	std::string token1 = imfat[atoi(contents[1].c_str())],
		token2 = imfat[atoi(contents[4].c_str())],
		token3 = imfat[atoi(contents[2].c_str())],
		token4 = imfat[atoi(contents[5].c_str())],
		combined_unhashed_token = token1 + token2 + token3 + token4,
		hashed_token = sha256(combined_unhashed_token);

	if (hashed_token == contents[3]) {
		//version = "premium";	//Standard Luxx Premium
		tsOut("Auth Success: Premium Version");
	}

	token1 = imfat[atoi(contents[4].c_str())];
	token2 = imfat[atoi(contents[1].c_str())];
	token3 = imfat[atoi(contents[5].c_str())];
	token4 = imfat[atoi(contents[2].c_str())];
	combined_unhashed_token = token1 + token2 + token3 + token4;
	hashed_token = sha256(combined_unhashed_token);

	if (hashed_token == contents[3]) {
		//version = "legacy";	//Legacy Only
		tsOut("Auth Success: Legacy Version");
	}

	token1 = imfat[atoi(contents[5].c_str())];
	token2 = imfat[atoi(contents[4].c_str())];
	token3 = imfat[atoi(contents[2].c_str())];
	token4 = imfat[atoi(contents[1].c_str())];
	combined_unhashed_token = token1 + token2 + token3 + token4;
	hashed_token = sha256(combined_unhashed_token);

	if (hashed_token == contents[3]) {
		//version = "elite";	//LUXX Elite (STAFF) + Recovery (include this in recovery version)
		tsOut("Auth Success: Elite Version");
	}

	/*if (contents[0] == hashed_hwid_string) {
	std::string token1 = imfat[atoi(contents[1].c_str())],
	token2 = imfat[atoi(contents[4].c_str())],
	token3 = imfat[atoi(contents[2].c_str())],
	token4 = imfat[atoi(contents[5].c_str())],
	combined_unhashed_token = token1 + token2 + token3 + token4,
	hashed_token = sha256(combined_unhashed_token);
	Log::Msg("Tokens + Hashes Done");

	if (hashed_token != contents[3]) {
	Log::Msg("Auth failed...");
	exit(0);
	is_cracked = true;
	}
	}
	else {
	Log::Msg("Auth failed...");
	exit(0);
	is_cracked = true;
	}*/
}

#pragma region example funcs
void testFunction1() {
	tsOut("function 1 enabled");
}
void testFunction2() {
	tsOut("function 2 enabled");
}
void testFunction3() {
	tsOut("function 3 enabled");
}
void testFunction4() {
	tsOut("function 4 enabled");
}
void testFunction5() {
	tsOut("function 5 enabled");
}
void testFunction6() {
	tsOut("function 6 enabled");
}
void testFunction7() {
	tsOut("function 7 enabled");
}
void testFunction8() {
	tsOut("function 8 enabled");
}
#pragma endregion

enum eFuncs {
	FUNCTION_1 = 1,
	FUNCTION_2,
	FUNCTION_3,
	FUNCTION_4,
	FUNCTION_5,
	FUNCTION_6,
	FUNCTION_7,
	FUNCTION_8,
};

bool remoteFuncs[]{ true, true, true, true, true, true, true, true, true, true };
void checkFuncs() {

/*	//read auth file and build post message
	std::string check = "request=" + static_cast<std::string>("functions"), request = check + "&" + verify;

	//recieve info matching notice
	std::wstring response = remoteServer.post(request);
	*/
	//cast wstring response to string
	std::string str_response = serverCall("functions");// str_response.assign(response.begin(), response.end());

	if (str_response.find("swagly ") == std::string::npos) {
		tsOut(str_response); //when using with luxx, change this to game::notifyError(str_response); and remove error from the start of the response.
		return;
	}
	else {
		//there must always be at least enough data received to cover the bools otherwise it'll crash
		std::size_t position = 0; std::string split_token; std::vector<std::string> contents;
		while ((position = str_response.find("b")) != std::string::npos) {
			split_token = str_response.substr(0, position);
			contents.push_back(split_token);
			str_response.erase(0, position + 1);
		}

		ostringstream cS;
		cS << sizeof(contents);
		tsOut("contents has "s + cS.str() + " members"s);

		contents.push_back(str_response);
		tsOut("Split into the vector");

		for (int i = 0; i < 8; i++) {
			ostringstream i2;
			i2 << (i + 1);
			//tsOut("function "s + i2.str() + " set to: "s + contents[i + 1]);
			if (contents[i + 1] == "0")
				remoteFuncs[i] = false;
			else if (contents[i + 1] == "1")
				remoteFuncs[i] = true;
			else
				remoteFuncs[i] = false;
		}
	}
}

int remotePrefs[]{ 0, 0, 0, 0, 0, 0, 0 };
string remotePrefN[]{ "Theme", "RiskyEnabled", "remotePref 3", "remotePref 4", "remotePref 5", "remotePref 6", };
void checkPreferences() {

	//read auth file and build post message
	std::string check = "request=" + static_cast<std::string>("preferences"), request = check + "&" + verify + "&" + tokenD;

	//recieve info matching notice
	std::wstring response = remoteServer.post(request);

	//cast wstring response to string
	std::string str_response; str_response.assign(response.begin(), response.end());

	if (str_response.find("prefs: ") == std::string::npos) {
		tsOut(str_response); //when using with luxx, change this to game::notifyError(str_response); and remove error from the start of the response.
		return;
	}
	else {
		std::size_t position = 0; std::string split_token; std::vector<std::string> contents;
		while ((position = str_response.find("p")) != std::string::npos) {
			split_token = str_response.substr(0, position);
			contents.push_back(split_token);
			str_response.erase(0, position + 1);
		}
		contents.push_back(str_response);
		tsOut("Split into the vector");

		for (int i = 0; i < 8; i++) {
			tsOut(remotePrefN[i] + " set to: "s + contents[i + 1]);
			remotePrefs[i] = atoi(contents[i].c_str());
		}
	}
}

void setPreferences() {

}

void drawTitle() {
	int dVal = 75;
	cout << "\n";
	Sleep(dVal);
	cout << "          _____            _____                                                          " << "\n" << std::flush;
	Sleep(dVal);
	cout << "         /\\    \\          /\\    \\                 ______                  ______          " << "\n" << std::flush;
	Sleep(dVal);
	cout << "        /::\\____\\        /::\\____\\               |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "       /:::/    /       /:::/    /               |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "      /:::/    /       /:::/    /                |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "     /:::/    /       /:::/    /                 |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "    /:::/    /       /:::/    /                  |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "   /:::/    /       /:::/    /                   |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "  /:::/    /       /:::/    /      _____         |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << " /:::/    /       /:::/____/      /\\    \\  ______|::|___|___ ____  ______|::|___|___ ____ " << "\n" << std::flush;
	Sleep(dVal);
	cout << "/:::/____/       |:::|    /      /::\\____\\|:::::::::::::::::|    ||:::::::::::::::::|    |" << "\n" << std::flush;
	Sleep(dVal);
	cout << "\\:::\\    \\       |:::|____\\     /:::/    /|:::::::::::::::::|____||:::::::::::::::::|____|" << "\n" << std::flush;
	Sleep(dVal);
	cout << " \\:::\\    \\       \\:::\\    \\   /:::/    /  ~~~~~~|::|~~~|~~~       ~~~~~~|::|~~~|~~~      " << "\n" << std::flush;
	Sleep(dVal);
	cout << "  \\:::\\    \\       \\:::\\    \\ /:::/    /         |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "   \\:::\\    \\       \\:::\\    /:::/    /          |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "    \\:::\\    \\       \\:::\\__/:::/    /           |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "     \\:::\\    \\       \\::::::::/    /            |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "      \\:::\\    \\       \\::::::/    /             |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "       \\:::\\____\\       \\::::/    /              |::|   |                |::|   |         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "        \\::/    /        \\::/____/               |::|___|                |::|___|         " << "\n" << std::flush;
	Sleep(dVal);
	cout << "         \\/____/          ~~                      ~~                      ~~              " << "\n\n" << std::flush;
	Sleep(dVal);
	cout << "\n";
	Sleep(dVal);
	cout << "Server Testing Tool V0.1" << "\n\n" << std::flush;
}

void drawTitleInverted() {
	cout << "Server Testing Tool V0.1" << "\n\n" << std::flush;
	cout << "\n";
	cout << "         \\/____/          ~~                      ~~                      ~~              " << "\n\n" << std::flush;
	cout << "        \\::/    /        \\::/____/               |::|___|                |::|___|         " << "\n" << std::flush;
	cout << "       \\:::\\____\\       \\::::/    /              |::|   |                |::|   |         " << "\n" << std::flush;

}

void mainSwitch() {

	options = 1;
	cout << "  __  __       _         __  __                  "s << "\n" << std::flush;
	cout << " |  \\/  |     (_)       |  \\/  |                 "s << "\n" << std::flush;
	cout << " | \\  / | __ _ _ _ __   | \\  / | ___ _ __  _   _ "s << "\n" << std::flush;
	cout << " | |\\/| |/ _` | | '_ \\  | |\\/| |/ _ \\ '_ \\| | | |"s << "\n" << std::flush;
	cout << " | |  | | (_| | | | | | | |  | |  __/ | | | |_| |"s << "\n" << std::flush;
	cout << " |_|  |_|\\__,_|_|_| |_| |_|  |_|\\___|_| |_|\\__,_|"s << "\n" << std::flush;
	cout << "                                                 "s << "\n" << std::flush;

	int switchIn = 0;
	tsOut("Please select which function you'd like to trigger");
	cout << "\n" << std::flush << "============================================================" << "\n\n" << std::flush;
	AddOption("Check if Server is Active"); AddOption("Store/Display HWID"); AddOption("Auth Test"); AddOption("testfunc4"); AddOption("Check is GTA 5 is updated"); AddOption("Check For Server Notices"); AddOption("Test Remote Function Disable"); AddOption("Test Cloud Preference Retreive");
	cout << "\n" << std::flush << "============================================================" << "\n" << std::flush;
	cin >> switchIn;

	switch (switchIn) {
	case FUNCTION_1: tsOut("Triggering Server Ping"); cout << "\n" << std::flush; if (remoteFuncs[switchIn]) { pingServer();	}
					 else tsOut("function 1 is false"); cout << "\n" << std::flush; system("pause"); system("cls"); mainSwitch(); break;
	case FUNCTION_2: tsOut("Triggering HWID Function"); cout << "\n" << std::flush;  if (remoteFuncs[switchIn]) { setHwid();	}
					 else tsOut("function 2 is false"); cout << "\n" << std::flush; system("pause");   system("cls"); mainSwitch(); break;
	case FUNCTION_3: tsOut("Triggering Auth Test"); cout << "\n" << std::flush;  if (remoteFuncs[switchIn]) { run_auth();	}
					 else tsOut("function 3 is false"); cout << "\n" << std::flush; system("pause");   system("cls"); mainSwitch(); break;
	case FUNCTION_4: tsOut("function 4 triggered"); cout << "\n" << std::flush;  if (remoteFuncs[switchIn]) { testFunction4();	}
					 else tsOut("function 4 is false"); cout << "\n" << std::flush; system("pause");   system("cls"); mainSwitch(); break;
	case FUNCTION_5: tsOut("Triggering GTA 5 Update Checker"); cout << "\n" << std::flush;  if (remoteFuncs[switchIn]) { checkUpdate();	}
					 else tsOut("function 5 is false"); cout << "\n" << std::flush; system("pause");   system("cls"); mainSwitch(); break;
	case FUNCTION_6: tsOut("Triggering Update Checker"); cout << "\n" << std::flush;  if (remoteFuncs[switchIn]) { checkNotices();	}
					 else tsOut("bool 6 is false"); cout << "\n" << std::flush; system("pause");   system("cls"); mainSwitch(); break;
	case FUNCTION_7: tsOut("Triggering Function Sync"); cout << "\n" << std::flush;  if (remoteFuncs[switchIn]) { checkFuncs();	}
					 else tsOut("bool 7 is false"); cout << "\n" << std::flush; system("pause");   system("cls"); mainSwitch(); break;
	case FUNCTION_8: tsOut("Triggering Pref Sync"); cout << "\n" << std::flush;  if (remoteFuncs[switchIn]) { checkPreferences(); }
					 else tsOut("bool 8 is false"); cout << "\n" << std::flush; system("pause");   system("cls"); mainSwitch(); break;
	default: tsOut("Invalid Request. . . \n Refreshing Menu"); Sleep(1500); system("cls"); mainSwitch(); break;
	}
	mainSwitch();
}

int main()
{
	system("pause");
	system("cls");
	drawTitle();
	system("pause");
	system("cls");
	mainSwitch();
	cout << "Invalid Request. Program will now close. . ." << "\n" << std::flush;
	Sleep(5000);
    return 0;
}

